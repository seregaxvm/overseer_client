library(shiny)
library(rhandsontable)

bootstrapLib(theme = "www/spacelab.css")
fluidPage(
  title = 'Overwatch',
  tabsetPanel(type = "tabs",
    tabPanel("Connection",
      sidebarLayout(
        sidebarPanel(
          textInput("ip", "Address", "server"),
          numericInput("port", "Port", value = 9090, min = 0, max = 65535, step = 1),
          br(),
          actionButton("connect", "Connect", class = "btn btn-success", icon = icon("link")),
          actionButton("disconnect", "Disconnect", class = "btn btn-danger", icon = icon("unlink"))
        ),
        mainPanel(
          textOutput("connectionStatus"),
          textInput("consol", "", placeholder = "type commands here"),
          actionButton("send", "Enter", class = "btn btn-success", icon = icon("check-square")),
          hr(),
          verbatimTextOutput("terminal", placeholder = TRUE)
        )
      )),
    tabPanel("Database",
             sidebarLayout(
               sidebarPanel(
                 textInput("db_ip", "Address", "database"),
                 numericInput("db_port", "Port", value = 3306, min = 0, max = 65535, step = 1),
                 textInput("db_user", "User", "root"),
                 textInput("db_pass", "Password", "password"),
                 textInput("db_name", "Database name", "data"),
                 br(),
                 actionButton("db_connect", "Connect", class = "btn btn-success", icon = icon("link")),
                 actionButton("db_disconnect", "Disconnect", class = "btn btn-danger", icon = icon("unlink"))
               ),
               mainPanel(
                 textOutput("dbStatus")
               )
             )),
    tabPanel("Table",
      sidebarLayout(
        sidebarPanel(
          fileInput(inputId = "inputFile",
                    label = "Control File",
                    multiple = FALSE,
                    accept = c("text/csv",
                               "text/comma-separated-values,text/plain",
                               ".csv"),
                    buttonLabel = icon("upload")),
          radioButtons("walktype",
                       "Walk over type",
                       choiceNames = list("Stop", "Start", "Oneshot"),
                       choiceValues = list("stop", "start", "oneshot"))
        ),
        mainPanel(
          rHandsontableOutput("control")
        )
    )),
    tabPanel("Plots",
             sidebarLayout(
               sidebarPanel(
                 selectInput("plot_states", label = "States", multiple = FALSE, choices = NULL),
                 selectInput("plot_client", label = "Clients", multiple = FALSE, choices = NULL),
                 hr(),
                 selectInput("plot_filter", label = "Filters", multiple = FALSE, choices = c("None", "Interpolate", "Approximate", "Custom")),
                 br(),
                 textAreaInput("text_filters", label = "Custom filter", placeholder = "Custom code here")
               ),
               mainPanel(
                 plotOutput("plots")
               )
             )),
    tabPanel("Postprocessing",
             sidebarLayout(
               sidebarPanel(
                 helpText("Under construction")
               ),
               mainPanel(
                 helpText("Under construction")
               )
             )),
    tabPanel("Image compilation",
             sidebarLayout(
               sidebarPanel(
                 helpText("Under construction")
               ),
               mainPanel(
                 helpText("Under construction")
               )
             ))
  )
)
